/*
 * Copyright (c) 2022-2023 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.martijn_heil.kbgl.dal.h2.repository

import me.martijn_heil.kbgl.dal.entity.Destroyable
import me.martijn_heil.kbgl.dal.repository.*
import java.sql.ResultSet
import javax.sql.DataSource

abstract class BaseH2KeyedRepository<K, T: Identifiable<K>>(private val keyClass: Class<K>, private val dataSource: DataSource)
	: MutableRepository<T>, KeyedRepository<K, T>, HasDirtyMarker<T> {
	override var dirtyMarker: DirtyMarker<T>? = null
	protected abstract val TABLE_NAME: String

	override fun all(): Collection<T> {
		return dataSource.connection.use { connection ->
			val stmnt = connection.prepareStatement("SELECT * FROM $TABLE_NAME")
			val result = stmnt.executeQuery()
			val all = ArrayList<T>()
			while (result.next()) {
				val parsed = fromRow(result)
				all.add(parsed)
			}
			all
		}
	}

	override fun destroy(v: T) {
		dataSource.connection.use { connection ->
			val stmnt = connection.prepareStatement("DELETE FROM $TABLE_NAME WHERE id = ?")
			stmnt.setObject(1, v.id)
			stmnt.executeUpdate()
		}
		if (v is Destroyable) v.isDestroyed = true
	}

	override fun forId(id: K): T? {
		return dataSource.connection.use { connection ->
			val stmnt = connection.prepareStatement("SELECT * FROM $TABLE_NAME WHERE id = ?")
			stmnt.setObject(1, id)
			val result = stmnt.executeQuery()
			if (!result.next()) return null
			fromRow(result)
		}
	}

	override fun allIds(): Collection<K> {
		return dataSource.connection.use { connection ->
			val stmnt = connection.prepareStatement("SELECT id FROM $TABLE_NAME")
			val result = stmnt.executeQuery()
			val all = ArrayList<K>()
			while (result.next()) all.add(result.getObject("id", keyClass))
			all
		}
	}

	protected abstract fun fromRow(row: ResultSet): T
}
