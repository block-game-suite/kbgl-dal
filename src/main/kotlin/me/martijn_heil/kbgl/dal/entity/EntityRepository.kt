/*
 * Copyright (c) 2022-2023 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.martijn_heil.kbgl.dal.entity

import me.martijn_heil.kbgl.dal.CacheMarker
import me.martijn_heil.kbgl.dal.repository.DirtyMarker
import me.martijn_heil.kbgl.dal.repository.Identifiable
import me.martijn_heil.kbgl.dal.repository.MutableRepository

interface EntityRepository<K, T: Identifiable<K>> : DirtyMarker<T>, MutableRepository<T>, CacheMarker<T> {
	fun forId(id: K): T?
	fun allIds(): Collection<K>
	fun saveAllDirty()
}
