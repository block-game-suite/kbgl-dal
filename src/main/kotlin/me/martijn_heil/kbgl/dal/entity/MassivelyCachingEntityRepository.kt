/*
 * Copyright (c) 2022-2023 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.martijn_heil.kbgl.dal.entity

import me.martijn_heil.kbgl.dal.repository.HasDirtyMarker
import me.martijn_heil.kbgl.dal.repository.Identifiable
import me.martijn_heil.kbgl.dal.repository.KeyedRepository
import me.martijn_heil.kbgl.dal.repository.MutableRepository
import java.lang.ref.SoftReference
import kotlin.concurrent.withLock

open class MassivelyCachingEntityRepository<K, T: Identifiable<K>, C>(child: C) : SimpleEntityRepository<K, T, C>(child)
		where C : KeyedRepository<K, T>,
			  C : MutableRepository<T>,
			  C: HasDirtyMarker<T> {

	override fun init() {
		super.init()
		this.all().forEach { this.markStrong(it) }
	}

	override fun create(v: T): T {
		val result = child.create(v)
		lock.writeLock().withLock {
			cache[result.id] = SoftReference(result)
		}
		this.markStrong(result)
		return result
	}
}
