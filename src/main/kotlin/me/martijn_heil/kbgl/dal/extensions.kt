/*
 * Copyright (c) 2022-2023 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.martijn_heil.kbgl.dal

import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.*


inline fun<reified T> ResultSet.getList(column: String): List<T>  {
	return (this.getArray(column).array as Array<*>).map { it as T }
}

inline fun<reified T> ResultSet.getList(column: Int): List<T>  {
	return (this.getArray(column).array as Array<*>).map { it as T }
}

inline fun<reified T> PreparedStatement.setList(n: Int, sqlDataType: String, collection: List<T>) {
	this.setArray(n, this.connection.createArrayOf("$sqlDataType ARRAY", collection.toTypedArray()))
}

inline fun<reified T> PreparedStatement.setCollection(n: Int, sqlDataType: String, collection: Collection<T>) {
	this.setArray(n, this.connection.createArrayOf("$sqlDataType ARRAY", collection.toTypedArray()))
}

fun ResultSet.getUuid(column: String) = this.getObject(column) as? UUID
fun ResultSet.getUuid(column: Int) = this.getObject(column) as? UUID
fun PreparedStatement.setUuid(n: Int, uuid: UUID?) = this.setObject(n, uuid)
