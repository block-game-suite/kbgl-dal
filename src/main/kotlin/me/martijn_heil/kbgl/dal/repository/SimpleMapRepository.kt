/*
 * Copyright (c) 2022-2023 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.martijn_heil.kbgl.dal.repository

import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.withLock

open class SimpleMapRepository<K, T: Identifiable<K>> : MutableRepository<T>, KeyedRepository<K, T> {
	protected val contents = HashMap<K, T>()
	protected val lock: ReadWriteLock = ReentrantReadWriteLock()

	override fun forId(id: K): T? = lock.readLock().withLock { contents[id] }
	override fun allIds(): Collection<K> = lock.readLock().withLock { contents.keys }
	override fun all(): Collection<T> = lock.readLock().withLock { contents.values }
	override fun destroy(v: T) { lock.writeLock().withLock { contents.remove(v.id) } }
	override fun create(v: T): T { lock.writeLock().withLock { contents[v.id] = v }; return v }
	override fun update(v: T) { lock.writeLock().withLock { contents[v.id] = v } }
}
