/*
 * Copyright (c) 2022-2023 Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.martijn_heil.kbgl.dal.repository

abstract class MappingRepository<KeyType, ChildType, ThisType, ChildRepositoryType>(private val child: ChildRepositoryType)
	: MutableRepository<ThisType>, KeyedRepository<KeyType, ThisType>

		where 	ThisType : Identifiable<KeyType>,
				ChildType : Identifiable<KeyType>,
			  	ChildRepositoryType: MutableRepository<ChildType>,
			  	ChildRepositoryType: KeyedRepository<KeyType, ChildType> {
	abstract fun convertFromChild(v: ChildType): ThisType
	abstract fun convertToChild(v: ThisType): ChildType
	override fun destroy(v: ThisType) = child.destroy(convertToChild(v))
	override fun create(v: ThisType): ThisType = convertFromChild(child.create(convertToChild(v)))
	override fun update(v: ThisType) = child.update(convertToChild(v))
	override fun allIds(): Collection<KeyType> = child.allIds()
	override fun all(): Collection<ThisType> = child.all().map { convertFromChild(it) }
	override fun forId(id: KeyType): ThisType? = child.forId(id)?.let { convertFromChild(it) }
}
