/*
 * Copyright (C) 2022-2023  Martijn Heil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.JavaVersion.VERSION_1_8

plugins {
	`java-gradle-plugin`
	`java-library`
    kotlin("jvm") version "1.8.10"
    idea
	`maven-publish`
}

group = "me.martijn_heil.kbgl.dal"
version = "1.0-SNAPSHOT"
description = "KBGL-DAL"

apply {
	plugin("java")
	plugin("kotlin")
	plugin("idea")
}

java {
	sourceCompatibility = VERSION_1_8
	targetCompatibility = VERSION_1_8
}


tasks {
	withType<ProcessResources> {
		filter(mapOf(Pair("tokens", mapOf(Pair("version", version)))), ReplaceTokens::class.java)
	}
}

repositories {
    mavenCentral()
    mavenLocal()
}

idea {
	module {
		isDownloadJavadoc = true
		isDownloadSources = true
	}
}

dependencies {
	implementation(kotlin("stdlib"))
}
